package Datos;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Formatter;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTextPane;
import javax.swing.filechooser.FileNameExtensionFilter;

public class DatosEditor {

    boolean comprobacionx = false;
    File arch;

    public void guardar(JTextPane txtArea) {
        FileNameExtensionFilter filtro = new FileNameExtensionFilter("Archivos de texto", "docx", "txt");
        JFileChooser archivo = new JFileChooser();
        archivo.setFileFilter(filtro);
        archivo.setDialogTitle("Admin Guardar......");
        int comparar = archivo.showSaveDialog(null);
        if (comparar == JFileChooser.APPROVE_OPTION) {
            final Formatter crear;
            try {
                crear = new Formatter(archivo.getSelectedFile() + ".docx");
                crear.format(txtArea.getText());
                crear.close();

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }

        }
    }

    public void abrirArch(JTextPane txtArea) {
        FileNameExtensionFilter filtro = new FileNameExtensionFilter("Archivos de texto", "docx", "txt", "doc");
        String nombre, contenido;
        JFileChooser archivo = new JFileChooser();
        archivo.setFileFilter(filtro);
        archivo.setDialogTitle("Archivos de Admin....");
        int comparar = archivo.showOpenDialog(null);

        if (comparar == JFileChooser.APPROVE_OPTION) {
            arch = archivo.getSelectedFile();
            nombre = arch.getName();

            try {

                FileInputStream f1 = new FileInputStream(arch);
                InputStreamReader f2 = new InputStreamReader(f1);
                BufferedReader linea = new BufferedReader(f2);
                contenido = "";
                while (linea.ready()) {
                    String linea_arch = linea.readLine();
                    contenido = contenido + linea_arch + "\n";
                }
                linea.close();
                txtArea.setText(contenido);

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

}
