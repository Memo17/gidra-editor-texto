package Visual;

import Datos.DatosEditor;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

/**
 *
 * @author ⋱⋱⋰⋰M€mo17⋱⋱⋰⋰
 */
public class FrmEditor extends javax.swing.JFrame {

    DatosEditor datos = new DatosEditor();

    private String fuentes[];
    private DefaultListModel dlm;

    public FrmEditor() throws UnsupportedLookAndFeelException {
        dlm = new DefaultListModel();
        fuentes = GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();
        initComponents();
        cerrar();
        lista.setModel(dlm);
        cargarComponentes();
        this.setIconImage(new ImageIcon(getClass().getResource("/Visual/Imagen1.png")).getImage());
    }

    private void cargarComponentes() {
        for (int i = 8; i <= 72; i = i + 2) {
            cmbTamaño.addItem(i);
        }
        for (String fuente : fuentes) {
            dlm.addElement(fuente);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnSave = new javax.swing.JButton();
        btnOpen = new javax.swing.JButton();
        btnNegrita = new javax.swing.JButton();
        btnCursiva = new javax.swing.JButton();
        btnSinFormato = new javax.swing.JButton();
        btnIzq = new javax.swing.JButton();
        btnCentrado = new javax.swing.JButton();
        btnDere = new javax.swing.JButton();
        cmbTamaño = new javax.swing.JComboBox();
        btnJustificar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        lista = new javax.swing.JList();
        btnCur_Neg = new javax.swing.JButton();
        jScrollPane4 = new javax.swing.JScrollPane();
        txtArea = new javax.swing.JTextPane();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("EDITOR TEXT");

        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Visual/save.png"))); // NOI18N
        btnSave.setToolTipText("");
        btnSave.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnSave.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnSave.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btnSave.setVerticalTextPosition(javax.swing.SwingConstants.TOP);
        btnSave.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSaveMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnSaveMousePressed(evt);
            }
        });
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        btnOpen.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Visual/open.png"))); // NOI18N
        btnOpen.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnOpen.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnOpen.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btnOpen.setVerticalTextPosition(javax.swing.SwingConstants.TOP);
        btnOpen.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnOpenMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnOpenMousePressed(evt);
            }
        });
        btnOpen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOpenActionPerformed(evt);
            }
        });

        btnNegrita.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Visual/neg.png"))); // NOI18N
        btnNegrita.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnNegrita.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnNegrita.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btnNegrita.setVerticalTextPosition(javax.swing.SwingConstants.TOP);
        btnNegrita.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnNegritaMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnNegritaMousePressed(evt);
            }
        });
        btnNegrita.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNegritaActionPerformed(evt);
            }
        });

        btnCursiva.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Visual/cur.png"))); // NOI18N
        btnCursiva.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnCursiva.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnCursiva.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btnCursiva.setVerticalTextPosition(javax.swing.SwingConstants.TOP);
        btnCursiva.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCursivaMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnCursivaMousePressed(evt);
            }
        });

        btnSinFormato.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Visual/Plain.png"))); // NOI18N
        btnSinFormato.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnSinFormato.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnSinFormato.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btnSinFormato.setVerticalTextPosition(javax.swing.SwingConstants.TOP);
        btnSinFormato.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnSinFormatoMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnSinFormatoMousePressed(evt);
            }
        });

        btnIzq.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Visual/left.png"))); // NOI18N
        btnIzq.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnIzq.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnIzq.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btnIzq.setVerticalTextPosition(javax.swing.SwingConstants.TOP);
        btnIzq.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnIzqMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnIzqMousePressed(evt);
            }
        });

        btnCentrado.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Visual/centered.png"))); // NOI18N
        btnCentrado.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnCentrado.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnCentrado.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btnCentrado.setVerticalTextPosition(javax.swing.SwingConstants.TOP);
        btnCentrado.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCentradoMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnCentradoMousePressed(evt);
            }
        });

        btnDere.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Visual/right.png"))); // NOI18N
        btnDere.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnDere.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnDere.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btnDere.setVerticalTextPosition(javax.swing.SwingConstants.TOP);
        btnDere.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnDereMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnDereMousePressed(evt);
            }
        });

        cmbTamaño.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        cmbTamaño.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbTamañoItemStateChanged(evt);
            }
        });

        btnJustificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Visual/justified.png"))); // NOI18N
        btnJustificar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnJustificar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnJustificar.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btnJustificar.setVerticalTextPosition(javax.swing.SwingConstants.TOP);
        btnJustificar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnJustificarMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnJustificarMousePressed(evt);
            }
        });

        jLabel1.setText("Tamaño");

        jLabel2.setText("Fuentes");

        lista.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                listaValueChanged(evt);
            }
        });
        jScrollPane2.setViewportView(lista);

        btnCur_Neg.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Visual/Neg_Cur.png"))); // NOI18N
        btnCur_Neg.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnCur_Neg.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnCur_Neg.setMaximumSize(new java.awt.Dimension(62, 48));
        btnCur_Neg.setMinimumSize(new java.awt.Dimension(62, 48));
        btnCur_Neg.setPreferredSize(new java.awt.Dimension(62, 48));
        btnCur_Neg.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btnCur_Neg.setVerticalTextPosition(javax.swing.SwingConstants.TOP);
        btnCur_Neg.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCur_NegMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnCur_NegMousePressed(evt);
            }
        });

        txtArea.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        txtArea.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        jScrollPane4.setViewportView(txtArea);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnOpen, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(30, 30, 30)
                        .addComponent(btnNegrita, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCursiva, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSinFormato, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCur_Neg, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 43, Short.MAX_VALUE)
                        .addComponent(btnIzq, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCentrado, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnDere, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnJustificar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(24, 24, 24)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cmbTamaño, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 228, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane4)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addComponent(btnIzq)
                        .addComponent(btnCentrado)
                        .addComponent(btnDere)
                        .addComponent(btnJustificar)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnSave)
                            .addComponent(btnOpen))
                        .addGroup(layout.createSequentialGroup()
                            .addGap(11, 11, 11)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(cmbTamaño, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel1)
                                .addComponent(jLabel2)))
                        .addComponent(btnCur_Neg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btnSinFormato)
                    .addComponent(btnCursiva)
                    .addComponent(btnNegrita))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 348, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnSaveActionPerformed

    private void cmbTamañoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbTamañoItemStateChanged
        Font f = txtArea.getFont();
        txtArea.setFont(new Font(f.getName(), Font.PLAIN, Integer.parseInt(String.valueOf(cmbTamaño.getSelectedItem()))));
    }//GEN-LAST:event_cmbTamañoItemStateChanged

    private void listaValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_listaValueChanged
        Font f = txtArea.getFont();
        txtArea.setFont(new Font(String.valueOf(dlm.getElementAt(lista.getSelectedIndex())), Font.PLAIN, f.getSize()));
    }//GEN-LAST:event_listaValueChanged

    private void btnOpenMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnOpenMousePressed
        datos.abrirArch(txtArea);
    }//GEN-LAST:event_btnOpenMousePressed

    private void btnNegritaMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnNegritaMousePressed
        Font f = txtArea.getFont();
        txtArea.setFont(new Font(f.getName(), Font.BOLD, f.getSize()));
    }//GEN-LAST:event_btnNegritaMousePressed

    private void btnCursivaMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCursivaMousePressed
        Font f = txtArea.getFont();
        txtArea.setFont(new Font(f.getName(), Font.ITALIC, f.getSize()));
    }//GEN-LAST:event_btnCursivaMousePressed

    private void btnSinFormatoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSinFormatoMousePressed
        Font f = txtArea.getFont();
        txtArea.setFont(new Font(f.getName(), Font.PLAIN, f.getSize()));
    }//GEN-LAST:event_btnSinFormatoMousePressed

    private void btnCur_NegMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCur_NegMousePressed
        Font f = txtArea.getFont();
        txtArea.setFont(new Font(f.getName(), 3, f.getSize()));
    }//GEN-LAST:event_btnCur_NegMousePressed

    private void btnIzqMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnIzqMousePressed
        StyledDocument doc = txtArea.getStyledDocument();
        SimpleAttributeSet center = new SimpleAttributeSet();
        StyleConstants.setAlignment(center, StyleConstants.ALIGN_LEFT);
        doc.setParagraphAttributes(0, doc.getLength(), center, false);
    }//GEN-LAST:event_btnIzqMousePressed

    private void btnOpenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOpenActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnOpenActionPerformed

    private void btnSaveMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaveMousePressed
        datos.guardar(txtArea);
    }//GEN-LAST:event_btnSaveMousePressed

    private void btnCentradoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCentradoMousePressed
        StyledDocument doc = txtArea.getStyledDocument();
        SimpleAttributeSet center = new SimpleAttributeSet();
        StyleConstants.setAlignment(center, StyleConstants.ALIGN_CENTER);
        doc.setParagraphAttributes(0, doc.getLength(), center, false);
    }//GEN-LAST:event_btnCentradoMousePressed

    private void btnDereMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnDereMousePressed
        StyledDocument doc = txtArea.getStyledDocument();
        SimpleAttributeSet center = new SimpleAttributeSet();
        StyleConstants.setAlignment(center, StyleConstants.ALIGN_RIGHT);
        doc.setParagraphAttributes(0, doc.getLength(), center, false);
    }//GEN-LAST:event_btnDereMousePressed

    private void btnJustificarMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnJustificarMousePressed
        StyledDocument doc = txtArea.getStyledDocument();
        SimpleAttributeSet center = new SimpleAttributeSet();
        StyleConstants.setAlignment(center, StyleConstants.ALIGN_JUSTIFIED);
        doc.setParagraphAttributes(0, doc.getLength(), center, false);
    }//GEN-LAST:event_btnJustificarMousePressed

    private void btnSaveMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSaveMouseEntered
        btnSave.setToolTipText("Guardar");
    }//GEN-LAST:event_btnSaveMouseEntered

    private void btnOpenMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnOpenMouseEntered
        btnOpen.setToolTipText("Abrir Archivo");
    }//GEN-LAST:event_btnOpenMouseEntered

    private void btnNegritaMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnNegritaMouseEntered
        btnNegrita.setToolTipText("Negrita");
    }//GEN-LAST:event_btnNegritaMouseEntered

    private void btnCursivaMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCursivaMouseEntered
        btnCursiva.setToolTipText("Cursiva");
    }//GEN-LAST:event_btnCursivaMouseEntered

    private void btnSinFormatoMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSinFormatoMouseEntered
        btnSinFormato.setToolTipText("Borrar Formato");
    }//GEN-LAST:event_btnSinFormatoMouseEntered

    private void btnCur_NegMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCur_NegMouseEntered
        btnCur_Neg.setToolTipText("Negrita y Cursiva");
    }//GEN-LAST:event_btnCur_NegMouseEntered

    private void btnIzqMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnIzqMouseEntered
        btnIzq.setToolTipText("Izquierda");
    }//GEN-LAST:event_btnIzqMouseEntered

    private void btnCentradoMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCentradoMouseEntered
        btnCentrado.setToolTipText("Centrar");
    }//GEN-LAST:event_btnCentradoMouseEntered

    private void btnDereMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnDereMouseEntered
        btnDere.setToolTipText("Derecha");
    }//GEN-LAST:event_btnDereMouseEntered

    private void btnJustificarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnJustificarMouseEntered
        btnJustificar.setToolTipText("Justificar");
    }//GEN-LAST:event_btnJustificarMouseEntered

    private void btnNegritaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNegritaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnNegritaActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCentrado;
    private javax.swing.JButton btnCur_Neg;
    private javax.swing.JButton btnCursiva;
    private javax.swing.JButton btnDere;
    private javax.swing.JButton btnIzq;
    private javax.swing.JButton btnJustificar;
    private javax.swing.JButton btnNegrita;
    private javax.swing.JButton btnOpen;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnSinFormato;
    private javax.swing.JComboBox cmbTamaño;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JList lista;
    private javax.swing.JTextPane txtArea;
    // End of variables declaration//GEN-END:variables

    public void cerrar() {
        try {
            this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
            addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent e) {
                    confirmarSalida();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void confirmarSalida() {
        int valor = JOptionPane.showConfirmDialog(null, "¿Desea Salir?", "Advertencia", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
        if (valor == JOptionPane.YES_OPTION) {
            JOptionPane.showMessageDialog(null, "GRACIAS POR UTILIZAR EL EIDTOR DE TEXTO GIDRA \n\nINTEGRANTES: \n\nBAQUERIZO MEJIA GUILLERMO \nBAQUERIZO MENDEZ DAYANA"
                    + "\nDELGADO VERGARA RONALD \nRAMOS OCHOA IVAN \nZAMBRANO MANZANO ADRIANA", "INFORMACION", JOptionPane.INFORMATION_MESSAGE);
            System.exit(0);
        }
    }
}
